---
layout: post
title:  "ns-3 App Store Launched"
date:   2018-09-24 13:55:18 -0700
categories: news
excerpt_separator: <!--more-->
---

We would like to announce the general availability of the [ns-3 App Store](https://apps.nsnam.org). The app store will organize information about the availability of extensions to the main ns-3 releases, starting with the current ns-3.29 release. The main page has more information about getting started with the app store. The app store implementation is largely due to **Abhijith Anilkumar’s 2017 Google Summer of Code project**, with backend extensions to Bake build system by **Ankit Deepak**, and with thanks to the **Cytoscape project** for permitting the reuse of [their framework](https://apps.cytoscape.org). The app store concept and initial Bake implementation are due to [Inria](http://team.inria.fr/diana) (thanks due to **Mathieu Lacage**, **Daniel Camara**, and **Walid Dabbous**).

