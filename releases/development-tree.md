---
id: 32
title: Development Tree
date: 2011-01-12T11:54:58+00:00
author: tomh
layout: page
guid: http://www2.nsnam.org/
---
ns-3 is developed and maintained with the [mercurial](http://mercurial.selenic.com/) source code management tool. Our code server is located [there](http://code.nsnam.org). The main source code repository is thus available either through the [web interface](http://code.nsnam.org/ns-3-dev) or via a command-line:

<pre>hg clone http://code.nsnam.org/ns-3-dev
</pre>