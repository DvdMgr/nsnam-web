---
title: Documentation
layout: page
permalink: /releases/ns-3-29/documentation/
---
  * **Tutorial:** an introduction into downloading, setting up, and using builtin models:[pdf](/docs/release/3.29/tutorial/ns-3-tutorial.pdf), [html (single page)](/docs/release/3.29/tutorial/singlehtml/index.html), [html (split page)](/docs/release/3.29/tutorial/html/index.html)
  * **Manual:** an in-depth coverage of the architecture and core of ns-3: [pdf](/docs/release/3.29/manual/ns-3-manual.pdf), [html (single page)](/docs/release/3.29/manual/singlehtml/index.html), [html (split page)](/docs/release/3.29/manual/html/index.html)
  * **Model Library:** documentation on individual protocol and device models that build on the ns-3 core: [pdf](/docs/release/3.29/models/ns-3-model-library.pdf), [html (single page)](/docs/release/3.29/models/singlehtml/index.html), [html (split page)](/docs/release/3.29/models/html/index.html)
  * [Release Errata](/wiki/Errata) 
  * [API Documentation](/docs/release/3.29/doxygen/index.html):&nbsp; Coverage of the C++ APIs using Doxygen.
  * Documentation of the [Bake](http://www.nsnam.org/docs/bake/tutorial/html/index.html) build tool
