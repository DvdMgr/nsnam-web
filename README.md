# ns3 minima

*ns3 minima is a custom Jekyll theme for the ns-3 Network Simulator website*. It's built off of the default Jekyll theme, Minima. It's uses the Foundation (v5) framework and is styled based off the GNU Octave site.

[Base Theme Minima](https://jekyll.github.io/minima/)

[ns3 theme based on GNU Octave theme](https://www.gnu.org/software/octave/)

There is very little custom theming needed. So when you’re adding content and need buttons or tabs, columns/grids, etc… you can refer to the Foundation docs here:

[Foundation 5 Documentation](https://foundation.zurb.com/sites/docs/v/5.5.3/)

## Setup

###Installation
Jekyll must be setup/configured on both local and production environments. For detailed setup instructions, please refer to the official documentation for your system:
[Installation Requirements](https://jekyllrb.com/docs/installation)

Once this is done you can clone the custom ns-3 theme to a local directory which will be your development environment to work on. Get the repo from this [Bitbucket repository](https://bitbucket.org/vanessagill/ns3/src/master/).

To launch the site preview, run
```bash
bundle exec jekyll serve
```
from within the folder where you cloned the site, and then navigate to http://localhost:4000 on your local environment to see it. You can see changes you make to the local files by refreshing the site in your browser after saving.

NOTE: If you update the header_pages parameter in the `_config.yml` it won't update automatically like other content. You'll have to close the live preview (control+C) and then start it again to see the changes take effect.

You can make edits to the page template files and add new pages and menus per the other instructions in the 'Usage' section of this README below.

### Deployment
Push your commits up to the repository on Bitbucket, and then pull down your changes to the production server. (Make sure you have setup Jekyll, Ruby, RubyGems, Bundler, Git on this server as well). You can clone the theme to any directory (recommend your home dir). Make sure your branch on the build machine is up to date with remote before compiling the source files each time you want to update the site.

*BEFORE COMPILING:* When your branch is up to date with origin, you must edit the `_config.yml` file before compiling to /var/www/html. Make sure that the 'url' parameter is set to the hostname and protocol for the site, ex: http://nsnam-bkp.coe-hosted.gatech.edu.

*IMPORTANT:* always remove the trailing slash from your hostname, if you keep the trailing slash it will break all of your menu page links.

Once this is set, save `_config.yml` and then run the command
```bash
bundle exec jekyll build --destination /var/www/html
```
to compile the site.

Navigate to the site in your browser and test to see that everything looks okay.


## Usage

### Home Layout
`index.html` is a flexible HTML layout for the site's home-page / index-page. Every thing on this page is mostly custom, and utilizes the Foundation 5 framework for its structure/grid. All text should be updated on this page with the exception of the Recent News column which is dynamically generated when a new post is added (via a new file in the `_posts` directory).

### Add new basic pages
Add each new page as a new .md or .html file in the root directory — ex: for the `about.md` page, I just copied/pasted the text from the “what is ns3” page on your current site, and tweaked it a bit for markdown. If you don’t want to use markdown files you can create regular html files as well and mix and match. Ex: `our-team.html`. This is helpful if you know you will be using html elements for features on a page, whereas markdown is easier for basic text.

Each page also contains "front matter" which needs to be set for each page and determines the page title, it’s permalink, as well as which layout it uses.

For sub-pages (secondary, tertiary, etc...): You save them in the root directory just like the primary pages, but make sure that their permalink in the front matter has the right structure depending on where the page will live, i.e. /support/running-scripts/ and not just /running-scripts/. Once you're done creating a new page, you have to define where it goes within the site by editing the sub-navigation in the `_data/nav.yml` file (instructions in README below under the "Customize sidebar submenu navigation links” section).

### Add new news posts
New news posts/pages should be added to the `_posts` folder, and their file name should mimic the format of the existing ones (year-month-day-title-of-post.markdown). Also be sure to add the front matter to the top of the file (can be copied and modified from an existing post). The <!--more--> tag can be placed anywhere in the actual post content in order to limit the amount of text for the excerpt that is displayed on the homepage News column.

### Change font
To replace the default site font (Google Webfont Open Sans) -- edit the `_includes/head.html` file and replace the google font link near the top with your new one. Then find and replace every instance of "Open Sans" in the `assets/css/app.css` file with your new font family name.

### Customize sidebar submenu navigation links
This allows you to set which pages you want to appear in the submenu navigation area in the sidebar of basic pages and configure order and hierarchy of the links.

Edit the `nav.yml` file in the `_data` directory. Nest sub-pages underneath their parent page with the `sub` parameter. Make sure to include the link title, and the href must match the permalink of the page:

```yaml
- title: Support
  href: /support/
  sub:
    - title: FAQ
      href: /support/faq/
    - title: Report a Bug
      href: /support/report-a-bug/
```

### Customize topbar navigation links
This allows you to set which pages you want to appear in the navigation area and configure order of the links.

For instance, to only link to the `about` and the `portfolio` page, add the following to you `_config.yml`:

```yaml
header_pages:
  - about.md
  - portfolio.md
```


## Contents At-A-Glance

### Layouts

Refers to files within the `_layouts` directory, that define the markup for your theme.

  - `default.html` -- The base layout that lays the foundation for subsequent layouts. The derived layouts inject their contents into this file at the line that says ` {{ content }} ` and are linked to this file via [FrontMatter](https://jekyllrb.com/docs/frontmatter/) declaration `layout: default`.
  - `home.html` -- The layout for your landing-page / home-page / index-page. [[More Info.](#home-layout)]
  - `page.html` -- The layout for your documents that contain FrontMatter, but are not posts.
  - `post.html` -- The layout for your posts.

### Includes

Refers to snippets of code within the `_includes` directory that can be inserted in multiple layouts (and another include-file as well) within the same theme-gem.

  - `disqus_comments.html` -- Code to markup disqus comment box.
  - `footer.html` -- Defines the site's footer section.
  - `google-analytics.html` -- Inserts Google Analytics module (active only in production environment).
  - `head.html` -- Code-block that defines the `<head></head>` in *default* layout.
  - `header_breadcrumb.html` -- Code-block that dynamically generates breadcrumb trail for all pages based on their permalink structure/hierarchy.
  - `header.html` -- Defines the site's main header section. By default, pages with a defined `title` attribute will have links displayed here.
  - `navigation.html` -- Code-block that recursively generates dynamic sub-page menu in left sidebar of basic pages, based on the configuration set in the `nav.yml` file within the `_data` directory.

### Sass

Refers to `.scss` files within the `_sass` directory that define the theme's styles.

  - `minima.scss` -- The core file imported by preprocessed `main.scss`, it defines the variable defaults for the theme and also further imports sass partials to supplement itself.
  - `minima/_base.scss` -- Resets and defines base styles for various HTML elements.
  - `minima/_layout.scss` -- Defines the visual style for various layouts.
  - `minima/_syntax-highlighting.scss` -- Defines the styles for syntax-highlighting.

### Assets

Refers to various asset files within the `assets` directory.
Contains the `main.scss` that imports sass files from within the `_sass` directory. This `main.scss` is what gets processed into the theme's main stylesheet `main.css` called by `_layouts/default.html` via `_includes/head.html`.

This directory can include sub-directories to manage assets of similar type, and will be copied over as is, to the final transformed site directory.

### Plugins

Minima comes with [`jekyll-seo-tag`](https://github.com/jekyll/jekyll-seo-tag) plugin preinstalled to make sure your website gets the most useful meta tags. See [usage](https://github.com/jekyll/jekyll-seo-tag#usage) to know how to set it up.


### Main Heading and Content-injection

From Minima v2.2 onwards, the *home* layout will inject all content from your `index.md` / `index.html` **before** the **`Posts`** heading. This will allow you to include non-posts related content to be published on the landing page under a dedicated heading. *We recommended that you title this section with a Heading2 (`##`)*.

Usually the `site.title` itself would suffice as the implicit 'main-title' for a landing-page. But, if your landing-page would like a heading to be explicitly displayed, then simply define a `title` variable in the document's front matter and it will be rendered with an `<h1>` tag.

### Post Listing

This section is optional from Minima v2.2 onwards.<br/>
It will be automatically included only when your site contains one or more valid posts or drafts (if the site is configured to `show_drafts`).

The title for this section is `Posts` by default and rendered with an `<h2>` tag. You can customize this heading by defining a `list_title` variable in the document's front matter.

--

### Customization

To override the default structure and style of minima, simply create the concerned directory at the root of your site, copy the file you wish to customize to that directory, and then edit the file.
e.g., to override the [`_includes/head.html `](_includes/head.html) file to specify a custom style path, create an `_includes` directory, copy `_includes/head.html` from minima gem folder to `<yoursite>/_includes` and start editing that file.

The site's default CSS has now moved to a new place within the gem itself, [`assets/main.scss`](assets/main.scss). To **override the default CSS**, the file has to exist at your site source. Do either of the following:
- Create a new instance of `main.scss` at site source.
  - Create a new file `main.scss` at `<your-site>/assets/`
  - Add the frontmatter dashes, and
  - Add `@import "minima";`, to `<your-site>/assets/main.scss`
  - Add your custom CSS.
- Download the file from this repo
  - Create  a new file `main.scss` at `<your-site>/assets/`
  - Copy the contents at [assets/main.scss](assets/main.scss) onto the `main.scss` you just created, and edit away!
- Copy directly from Minima 2.0 gem
  - Go to your local minima gem installation directory ( run `bundle show minima` to get the path to it ).
  - Copy the `assets/` folder from there into the root of `<your-site>`
  - Change whatever values you want, inside `<your-site>/assets/main.scss`

--

### Change default date format

You can change the default date format by specifying `site.minima.date_format`
in `_config.yml`.

```
# Minima date format
# refer to http://shopify.github.io/liquid/filters/date/ if you want to customize this
minima:
  date_format: "%b %-d, %Y"
```

--

### Enabling comments (via Disqus)

Optionally, if you have a Disqus account, you can tell Jekyll to use it to show a comments section below each post.

To enable it, add the following lines to your Jekyll site:

```yaml
  disqus:
    shortname: my_disqus_shortname
```

You can find out more about Disqus' shortnames [here](https://help.disqus.com/customer/portal/articles/466208).

Comments are enabled by default and will only appear in production, i.e., `JEKYLL_ENV=production`

If you don't want to display comments for a particular post you can disable them by adding `comments: false` to that post's YAML Front Matter.

--

### Social networks

You can add links to the accounts you have on other sites, with respective icon, by adding one or more of the following options in your config:

```yaml
twitter_username: jekyllrb
github_username:  jekyll
dribbble_username: jekyll
facebook_username: jekyll
flickr_username: jekyll
instagram_username: jekyll
linkedin_username: jekyll
pinterest_username: jekyll
youtube_username: jekyll
googleplus_username: +jekyll
rss: rss

mastodon:
 - username: jekyll
   instance: example.com
 - username: jekyll2
   instance: example.com
```

--

### Enabling Google Analytics

To enable Google Anaytics, add the following lines to your Jekyll site:

```yaml
  google_analytics: UA-NNNNNNNN-N
```

Google Analytics will only appear in production, i.e., `JEKYLL_ENV=production`

--

### Enabling Excerpts on the Home Page

To display post-excerpts on the Home Page, simply add the following to your `_config.yml`:

```yaml
show_excerpts: true
```

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/jekyll/minima. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## Development

To set up your environment to develop this theme, run `script/bootstrap`.

To test your theme, run `script/server` (or `bundle exec jekyll serve`) and open your browser at `http://localhost:4000`. This starts a Jekyll server using your theme and the contents. As you make modifications, your site will regenerate and you should see the changes in the browser after a refresh.

## License

The theme is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).
