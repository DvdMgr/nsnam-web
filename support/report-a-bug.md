---
title: Report a bug
layout: page
permalink: /support/report-a-bug/
---

There are two primary ways to log bug reports, patches, or feature requests:

  1. email the [ns-developers](/support/mailing-lists) mailing list (you will need to subscribe to the list to be able to post, and first-time posts are moderated); or
  2. enter the bug into the [bugzilla](/bugzilla) database yourself

We strongly prefer the second approach; email to the mailing list runs the risk of falling through the cracks. The second approach requires you to [create a Bugzilla account](/bugzilla/), which can no longer be done automatically but can be set up by requesting an account from  [webmaster@nsnam.org](mailto:webmaster@nsnam.org?subject=Bugzilla%20account%20request).

If you think you've found a bug and want to report it, please do the following three things first:

  1. [search bugzilla](/bugzilla/index.cgi) to see if someone has entered a similar bug.
  2. [search Google](http://www.google.com/) to see if the bug has been previously encountered and discussed on an email thread
  3. if you are new to reporting bugs, please read the [bug writing guidelines](/bugzilla/page.cgi?id=bug-writing.html) on Bugzilla

Fixing bugs is even more preferred than reporting them. This is typically done by uploading a patch on an existing bug, or filing a bug and immediately filing a patch on it. If you are not familiar with how to prepare patch files, please [read this first](http://cocoon.apache.org/2.0/howto/howto-patch.html). You should find the "Create a New Attachment" choice when editing the bug, allowing you to upload a patch. Alternatively, you could provide a URL to a patch in the "Description" field.

Note that providing a simple, reduced test case that reliably reproduces the bug is often requested by the maintainers, so please try to craft such a test case and upload it to the tracker.
