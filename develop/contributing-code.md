---
title: Contributing Code
layout: page
permalink: /develop/contributing-code/
---
This document summarizes the steps to take to prepare code for submission to ns-3 as a new feature, such as a new feature of an existing simulation model, or a new model altogether. If you are interested in submitting a bugfix to an existing ns-3 feature, see the companion section [How to submit a bug report](/support/report-a-bug) to ns-3.

We actively encourage submission of new features to ns-3. Independent submissions are essential for open source projects, and you will also be credited as an author of future versions of ns-3. However, please keep in mind that there is already a large burden on the ns-3 maintainers to manage the flow of incoming contributions and maintain new and existing code. The goal of this document is thus to outline how you can help to minimize this burden and thus minimize the average time-to-merge of your code. Making sure that each code submission fulfills as many items as possible in the following checklist is the best way to ensure quick merging of your code.

In brief, we can summarize the guidelines as follows:

  1. Understand what a [code review](/developers/contributing-code/code-reviews) really is
  2. Be [licensed](/developers/contributing-code/licensing) appropriately
  3. Follow the ns-3 [coding style and engineering guidelines](/developers/contributing-code/coding-style)
  4. Write associated [documentation, tests, examples](/developers/contributing-code/supporting-material)
  5. [Prepare](/developers/contributing-code/get-ready) carefully your submission
  6. Pick a [submission tool](/developers/contributing-code/submit)

If you do not have the time to follow through the process to include your code in the main tree, please see [out of tree](/developers/contributing-code/out-of-tree) about contributing ns-3 code that is not maintained in the main tree.
