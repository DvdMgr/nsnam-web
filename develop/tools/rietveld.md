---
title: Rietveld
layout: page
permalink: /develop/tools/rietveld/
---
[Rietveld](http://code.google.com/p/rietveld/) is a web-based code review tool that has been built by google. It was originally developed as a demonstration of the google app engine but is now widely used by many open source projects to conduct code reviews in the open.

To use rietveld to request a code review for ns-3, one needs to upload a patch/changeset [here](http://codereview.appspot.com/). There are many ways to do this but we recommend that you follow these steps:

  1. Download [upload.py](http://codereview.appspot.com/static/upload.py).
  2. Record the changes you want to request a review for in a [mercurial](/developers/tools/mercurial) repository: <tt>hg commit ...</tt>
  3. Within the mercurial repository, run <tt>upload.py</tt>. Make sure you specify <tt>ns-3-reviews@googlegroups.com as a CC</tt>
  4. Announce your review request on ns-developers

If you already pulled changes from ns-3-dev into your private repository after you started doing your private modifications, there is an issue to be considered. upload.py does not let you specify a range of revisions, nor a set of changesets. So if you just run upload.py in your private repository, the changesets pulled from ns-3-dev will also be published on codereview, which is, of course, not desirable.

A possible workaround is to pull your changes into a temporary repository which is an up-to-date clone of ns-3-dev. The following code should do the trick. This code assumes that your private repository is in path DEV\_BRANCH\_WITH\_NEW\_FEATURE, and that it is in sync with ns-3-dev.

<pre>hg clone http://code.nsnam.org/ns-3-dev ns-3-tmp
cd ns-3-tmp
export REVNO=`hg tip -q | sed 's/:.*$//'`
hg pull DEV_BRANCH_WITH_NEW_FEATURE
hg merge
hg commit -m "merged new feature"
python upload.py --rev=$REVNO --cc=ns-3-reviews@googlegroups.com --oauth2
</pre>

To update a previous issue with a new patchset, use the -i (&#8211;issue) option to avoid the tool creating a new issue. For instance, if the issue to which you want to add a new patchset is numbered 165087, you would do

<pre>hg commit -m "merged new feature"
python upload.py --rev=$REVNO --issue=165087 --cc=ns-3-reviews@googlegroups.com --oauth2
</pre>

See also this [wiki HOWTO](https://www.nsnam.org/wiki/index.php/HOWTO_control_Rietveld_patch_generation) for more guidance.
