---
title: Waf
layout: page
permalink: /develop/tools/waf/
---
waf is a python-based build tool. Extensive information about how waf works, how it can be used can be found online from its [website](http://code.google.com/p/waf/). Most notably, users will find the so-called [Waf book](http://waf.googlecode.com/svn/docs/wafbook/single.html) as well as [API documentation](http://waf.googlecode.com/svn/docs/apidocs/index.html) about waf.

Information about how waf is used in ns-3 can be found [online](http://code.nsnam.org/ns-3-dev/raw-file/tip/doc/build.txt) too.
