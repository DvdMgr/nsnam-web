---
title: Mailing lists
layout: page
permalink: /develop/tools/mailing-lists/
---
The project maintains mailing lists which are targeted at developers:

  * <a name="ns-developers"></a>ns-developers: ns-3 development discussions. [Join](http://www.isi.edu/nsnam/ns/ns-dev-list.html)&nbsp;|&nbsp;[Archives](http://mailman.isi.edu/pipermail/ns-developers/) 
      * <a name="ns-3-reviews"></a>ns-3-reviews: ns-3 code reviews. [Join | Archives](http://groups.google.com/group/ns-3-reviews) 
          * <a name="ns-commits"></a>ns-commits: every mercurial commit pushed to one of the repositories hosted on code.nsnam.org generates an email describing the commit, and nightly validation messages. [Join](http://mailman.isi.edu/mailman/listinfo/ns-commits)&nbsp;|&nbsp;[Archives](http://mailman.isi.edu/piper
mail/ns-commits/) 
              * <a name="ns-bugs"></a>ns-bugs: Automatic mail from Bugzilla database updates. [Join](http://mailman.isi.edu/mailman/listinfo/ns-bugs)&nbsp;|&nbsp;[Archives](http://mailman.isi.edu/pipermail/ns-bugs/) </ul> 
                For user-oriented mail (getting help with using ns-3) there is also the <a name="ns-3-users"></a>**ns-3-users** mailing list: [Google Groups page](https://groups.google.com/forum/?fromgroups#!forum/ns-3-users)
