---
title: Submit
layout: page
permalink: /develop/contributing-code/submit/
---
There are many ways to submit a piece of code. Some of them are not appropriate:

  * do not send us a modified version of each file you have changed
  * do not send us a .zip or .tar.gz file by email with a copy of every modified file
  * do not send us a patch against an unspecified version of ns-3

Others are more appropriate:

  1. a simple patch
  2. patch reviews through [rietveld](/developers/tools/rietveld).
  3. a hosted clone

Most submissions to ns-3 use one of the above techniques.

# a simple patch

Patches can be attached to a bug report or sent to our developer mailing-lists.

The UNIX <tt>diff</tt> tool is the most common way of producing a patch: a patch is a text-based representation of the difference between two text files or two directories with text files in them. If you have two files, <tt>original.cc</tt>, and, <tt>modified.cc</tt>, you can generate a patch with the command <tt>diff -u original.cc modified.cc</tt>. If you wish to produce a patch between two directories, use the command <tt>diff -uprN original modified</tt>.

Make sure you specify to the reviewer where the original files came from and make sure that the resulting patch file does not contain unexpected content by performing a final inspection of the patch file yourself.

Patches such as this are sufficient for simple bug fixes or very simple small features.

# Patch reviews through [rietveld](/developers/tools/rietveld)

The best way to submit a larger patch for consideration is to request a patch review through [rietveld](/developers/tools/rietveld).

# Hosted clones

If your bundles grow large in size, it can become tricky to send them over email: hosting a clone of your mercurial repository is a simple way to work around this. If you have a host with a static ip address and/or a hostname, you can run <tt>hg serve</tt> within your repository in this host and send instead to your co-workers a url to that repository: if I run <tt>hg serve -d -p 8080</tt> on my local ns-3-dev clone, I could send the url <tt>http://diese.inria.fr:8080/</tt> to my co-workers to allow them to pull all my development history from there.

If you don&#8217;t have a static ip address or hostname, or are located behind a NAT or a firewall, you have to use a third-party hosting provider for your code. A small list of hosting providers is available [there](http://www.selenic.com/mercurial/wiki/index.cgi/MercurialHosting)

We can also provide hosting to ns-3 developers on [code.nsnam.org](http://code.nsnam.org/) should none of the above alternative work for you.
