---
layout: page
title: Research
permalink: /research/
---

This page is a placeholder for items concerning the use of ns-3 to support academic or industrial research.  

ns-3 has been used in thousands of publications to date.  To get a feel for the topics studied, search on the keywords 'ns-3 simulator' at the following:

* [Google Scholar](https://scholar.google.com) 
* [IEEE digital library](https://ieeexplore.ieee.org)
* [ACM digital library](https://dl.acm.org) 
